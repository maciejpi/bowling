package com.maciejp.bowling.frame;

import com.maciejp.bowling.exception.ResultUnavailableException;

import java.util.ArrayList;
import java.util.List;

public final class LastFrame implements Frame {

    private final List<Roll> ballsThrown = new ArrayList<>();

    public LastFrame(Roll rollThrow) {
        ballsThrown.add(rollThrow);
    }

    @Override
    public boolean isCompleted() {
        return (ballsThrown.size() == 2 && !firstThrowIsStrike() && !secondThrowIsSpare()) || ballsThrown.size() == 3;
    }

    private boolean secondThrowIsSpare() {
        return  ballsThrown.stream().mapToInt(Roll::getPinfall).sum() == 10;
    }

    private boolean firstThrowIsStrike() {
        return ballsThrown.get(0).getPinfall() == 10;
    }

    @Override
    public int score(List<Roll> rolls) {
        if(isCompleted()) {
            return ballsThrown.stream().mapToInt(Roll::getPinfall).sum();
        } else {
            throw new ResultUnavailableException("Not enough rolls to calculate frame result.");
        }
        }

    @Override
    public void addRoll(Roll roll) {
        ballsThrown.add(roll);
    }
}
