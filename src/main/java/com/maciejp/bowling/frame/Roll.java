package com.maciejp.bowling.frame;

import lombok.Value;

@Value
public class Roll {

   int thrownAs;
   int pinfall;
}
