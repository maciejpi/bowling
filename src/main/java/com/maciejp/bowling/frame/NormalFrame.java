package com.maciejp.bowling.frame;

import com.maciejp.bowling.exception.ResultUnavailableException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public final class NormalFrame implements Frame {

    private final List<Roll> ballsThrown = new ArrayList<>();

    public NormalFrame(Roll rollThrow) {
        ballsThrown.add(rollThrow);
    }

    @Override
    public boolean isCompleted() {
        return isStrike() || ballsThrown.size() == 2;
    }

    @Override
    public int score(List<Roll> rolls) throws ResultUnavailableException{
        checkIfScoreCanBeCalculated(rolls);

        int score;
        if(isStrike()) {
            Predicate<Roll> ballPredicate = roll -> roll.getThrownAs() == orderOfFirstBall() + 1 || roll.getThrownAs() == orderOfFirstBall() + 2;

            int sum = rolls.stream()
                    .filter(ballPredicate)
                    .mapToInt(Roll::getPinfall)
                    .sum();
            score = 10 + sum;
        } else if (isSpare()) {
            score = 10 + rolls.get(orderOfFirstBall() + 2).getPinfall();
        } else {
            score = ballsThrown.stream().mapToInt(Roll::getPinfall).sum();
        }

        return score;
    }

    private void checkIfScoreCanBeCalculated(List<Roll> rolls) {
        boolean notEnoughToCalculateStrike = isStrike() && rolls.size() <= orderOfFirstBall() + 2;
        boolean notEnoughToCalculateSpare = isSpare() && rolls.size() <= orderOfFirstBall() + 1;
        if(!isCompleted() || notEnoughToCalculateStrike || notEnoughToCalculateSpare) {
            throw new ResultUnavailableException("Not enough rolls to calculate frame result.");
        }
    }

    @Override
    public void addRoll(Roll roll) {
        ballsThrown.add(roll);
    }

    private boolean isStrike() {
        return ballsThrown.size() == 1 && ballsThrown.get(0).getPinfall() == 10;
    }

    private boolean isSpare() {
        return ballsThrown.size() == 2 && ballsThrown.stream().mapToInt(Roll::getPinfall).sum() == 10;
    }

    private int orderOfFirstBall() {
        return ballsThrown.get(0).getThrownAs();
    }
}
