package com.maciejp.bowling.frame;

import com.maciejp.bowling.exception.ResultUnavailableException;

import java.util.List;

public interface Frame {
    boolean isCompleted();

    int score(List<Roll> ballsThrown) throws ResultUnavailableException;

    void addRoll(Roll roll);
}
