package com.maciejp.bowling;

import com.maciejp.bowling.exception.GameFinishedException;
import com.maciejp.bowling.frame.Frame;
import com.maciejp.bowling.frame.LastFrame;
import com.maciejp.bowling.frame.NormalFrame;
import com.maciejp.bowling.frame.Roll;

import java.util.ArrayList;
import java.util.List;

public final class Game {

    private final List<Roll> ballsThrown = new ArrayList<>();
    private final List<Frame> frames = new ArrayList<>();

    public int score() {
        return score(frames.size());
    }

    public int score(int frameNumber) {
        return frames.stream()
                .limit(frameNumber)
                .mapToInt(frame -> frame.score(ballsThrown))
                .sum();
    }

    public void throwBall(int pinfall) throws GameFinishedException {
        checkIfGameIsFinished();
        addRoll(pinfall);
    }

    private void checkIfGameIsFinished() throws GameFinishedException {
        if (frames.size() == 10 && frameCompleted()) {
            throw new GameFinishedException("Can't add next throw, game finished!");
        }
    }

    private void addRoll(int pinfall) {
        Roll roll = new Roll(ballsThrown.size(), pinfall);
        ballsThrown.add(roll);
        if (frameCompleted()) {
            addNewFrame(roll);
        } else {
            addToFrame(roll);
        }
    }

    private boolean frameCompleted() {
        return frames.isEmpty() || frames.get(frames.size() - 1).isCompleted();
    }

    private void addNewFrame(Roll roll) {
        if (frames.size() == 9) {
            frames.add(new LastFrame(roll));
        } else {
            frames.add(new NormalFrame(roll));
        }
    }

    private void addToFrame(Roll roll) {
        Frame lastFrame = frames.get(frames.size() - 1);
        lastFrame.addRoll(roll);
    }
}

