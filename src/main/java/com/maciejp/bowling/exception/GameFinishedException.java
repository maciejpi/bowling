package com.maciejp.bowling.exception;

public class GameFinishedException extends Exception {
    public GameFinishedException(String message) {
        super(message);
    }
}
