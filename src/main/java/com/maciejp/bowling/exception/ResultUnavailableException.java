package com.maciejp.bowling.exception;

public class ResultUnavailableException extends RuntimeException {
    public ResultUnavailableException(String message) {
        super(message);
    }
}
