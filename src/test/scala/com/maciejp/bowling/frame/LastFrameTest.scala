package com.maciejp.bowling.frame

import com.maciejp.bowling.exception.ResultUnavailableException
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.JavaConverters._

class LastFrameTest extends FlatSpec with Matchers {

  "Last Frame" should "be completed if the score is lower than 10 after two throws" in {
    val frame = new LastFrame(new Roll(0, 2))
    frame.addRoll(new Roll(1, 3))

    frame.isCompleted should be(true)
  }

  it should "be completed after three throws if the first throw was a strike" in {
    val frame = new LastFrame(new Roll(0, 10))
    frame.addRoll(new Roll(1, 3))
    frame.addRoll(new Roll(2, 3))

    frame.isCompleted should be(true)
  }

  it should "be completed after three throws if the second throw was a spare" in {
    val frame = new LastFrame(new Roll(0, 1))
    frame.addRoll(new Roll(1, 9))
    frame.addRoll(new Roll(2, 3))

    frame.isCompleted should be(true)
  }

  it should "not be completed if there was only one throw" in {
    val frame = new LastFrame(new Roll(0, 2))

    frame.isCompleted should be(false)
  }

  it should "not be completed if there were two throws and first was a strike" in {
    val frame = new LastFrame(new Roll(0, 10))
    frame.addRoll(new Roll(1, 1))

    frame.isCompleted should be(false)
  }

  it should "not be completed if there were two throws and second was a spare" in {
    val frame = new LastFrame(new Roll(0, 1))
    frame.addRoll(new Roll(1, 9))

    frame.isCompleted should be(false)
  }

  it should "calculate the score as a sum of the throws it contains" in {
    val frame = new LastFrame(new Roll(0, 10))
    frame.addRoll(new Roll(1, 9))
    frame.addRoll(new Roll(2, 8))

    frame.score(Nil.asJava) should be(27)
  }

  it should "throw exception when score is requested before completing the frame" in {
    val frame = new LastFrame(new Roll(0,1))

    an [ResultUnavailableException] should be thrownBy frame.score(Nil.asJava)
  }
}
