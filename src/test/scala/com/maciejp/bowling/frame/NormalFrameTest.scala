package com.maciejp.bowling.frame

import com.maciejp.bowling.exception.ResultUnavailableException
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.JavaConverters._

class NormalFrameTest extends FlatSpec with Matchers {

  "Normal Frame" should "be completed if first roll is a strike" in {
    val frame = new NormalFrame(new Roll(0, 10))

    frame.isCompleted should be(true)
  }

  it should "be completed if the frame contains two rolls" in {
    val frame = new NormalFrame(new Roll(0, 1))
    frame.addRoll(new Roll(1, 4))

    frame.isCompleted should be(true)

  }

  it should "not be completed if the frame contains one roll that is not a strike" in {
    val frame = new NormalFrame(new Roll(0, 1))

    frame.isCompleted should be(false)
  }

  it should "calculate correct score with two non-spare rolls" in {
    val frame = new NormalFrame(new Roll(0, 1))
    frame.addRoll(new Roll(1, 4))

    frame.score(Nil.asJava) should be(5)
  }

  it should "calculate correct score with spare roll" in {
    val firstRoll = new Roll(0, 6)
    val secondRoll = new Roll(1, 4)
    val thirdRoll = new Roll(2, 5)
    val frame = new NormalFrame(firstRoll)
    frame.addRoll(secondRoll)

    frame.score(List(firstRoll, secondRoll, thirdRoll).asJava) should be(15)
  }

  it should "calculate correct score with strike" in {
    val firstRoll = new Roll(0, 10)
    val secondRoll = new Roll(1, 0)
    val thirdRoll = new Roll(2, 1)
    val frame = new NormalFrame(firstRoll)

    frame.score(List(firstRoll, secondRoll, thirdRoll).asJava) should be(11)
  }

  it should "throw cexception when score is requested before completing the frame" in {
    val frame = new LastFrame(new Roll(0,1))

    an [ResultUnavailableException] should be thrownBy frame.score(Nil.asJava)
  }

  it should "throw exception when the rolls required to calculate the score are not available" in {
    val frame = new LastFrame(new Roll(0,10))

    an [ResultUnavailableException] should be thrownBy frame.score(Nil.asJava)
  }
}
