package com.maciejp.bowling

import com.maciejp.bowling.exception.GameFinishedException
import org.scalatest.{FlatSpec, Matchers}

class GameTest extends FlatSpec with Matchers {

  "Game" should "have score 0 at the beginning" in {
    new Game().score() should be(0)
  }

  it should "calculate score after one throw" in {
    val game = new Game()
    game.throwBall(1)

    game.score() should be(1)
  }

  it should "calculate score after two throws" in {
    val game = new Game()
    game.throwBall(1)
    game.throwBall(4)

    game.score() should be(5)
  }

  it should "calculate score up to a given frame" in {
    val game: Game = gameWithGivenNumberOfOnePointThrows(4)

    game.score(2) should be(4)
  }

  it should "not allow specifying 21 throws" in {
    val game: Game = gameWithGivenNumberOfOnePointThrows(20)

    an [GameFinishedException] should be thrownBy game.throwBall(10)
  }

  it should "allow specifying 21 throws if the first throw of the last frame was a strike" in {
    val game: Game = gameWithGivenNumberOfOnePointThrows(18)
    game.throwBall(10)
    game.throwBall(10)
    game.throwBall(10)

    game.score() should be(48)
  }

  it should "allow specifying 21 throws if the second throw of the last frame was a spare" in {
    val game: Game = gameWithGivenNumberOfOnePointThrows(18)
    game.throwBall(6)
    game.throwBall(4)
    game.throwBall(10)

    game.score() should be(38)
  }

  private def gameWithGivenNumberOfOnePointThrows(numberOfThrows: Int) = {
    val game = new Game()
    for (i <- 1 to numberOfThrows) {
      game.throwBall(1)
    }
    game
  }
}
