package steps

import com.maciejp.bowling.Game
import cucumber.api.scala.{EN, ScalaDsl}

import scala.collection.TraversableLike

class BowlingScoringSteps extends ScalaDsl with EN {

  var game: Game = _

  When("""^the following rolls (.+)$""") { (rolls: String) =>
    game = new Game()

    csvToIntArray(rolls)
      .foreach(game.throwBall)
  }

  When("""^a strike is rolled in every frame$""") { () =>
    game = new Game()

    1 to 12 foreach { _ => game.throwBall(10) }
  }

  Then("""^the frame scores are (.+)$""") { (frames: String) =>
    val result = csvToIntArray(frames)
      .zipWithIndex
      .forall { case (x, i) => game.score(i + 1) == x }

    assert(result)
  }

  Then("""^the score is (\d+)$""") { (score: Int) =>
    assert(game.score() == score)
  }

  def csvToIntArray(input: String): Array[Int] = {
    input
      .replaceAll("\\s+", "")
      .split(Array(',', ';'))
      .map(_.toInt)
  }
}
