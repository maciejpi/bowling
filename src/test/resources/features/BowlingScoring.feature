Feature: Bowling game scoring algorithm

  Scenario: Calculate results of all frames in the example game from the specification
    When the following rolls 1,4 ; 4,5 ; 6,4 ; 5,5 ; 10 ; 0,1 ; 7,3 ; 6,4 ; 10 ; 2,8,6
    Then the frame scores are 5 , 14 , 29 , 49 , 60 , 61, 77, 97, 117, 133

    Scenario: The score of perfect game is 300
      When a strike is rolled in every frame
      Then the score is 300